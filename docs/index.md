# LINKping

Welcome to LINKping. We're a tiny hackerspace located in the city center of Linköping, Sweden.

## Why?

Originally started by `rtn` during a time of burn out and depression. Shortly before the whole covid-19 ordeal began.

## What?

Some areas that we're interested in:

* Programming
* Electronics
* Soldering
* 3D-printing
* [Wargames](https://en.wikipedia.org/wiki/Wargame_(hacking)) and [CTFs](https://en.wikipedia.org/wiki/Capture_the_flag#Computer_security)
* Mathematics

## Who?

* `rtn` node.js developer, interested in: free software, linux, emacs, micro controllers and wargames
* `gnyrfta` physics and mathematics teacher
* `filwe` math major

## Where?

You can find us on [`Delta Chat`](https://delta.chat) in our [public group](https://i.delta.chat/#0B1016987BC1746EDCAA21D262C60DE0D0BC414D&a=rtn%40linkping.org&g=linkping%20%2D%20public&x=Z-Se6WJ7N05atcN-lD5LaU5G&i=iLUuIPN4zviAfkTxFRS3ejOS&s=b-IWsE-LIjmNhEMn8V9gtqSE).
