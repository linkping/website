# Markdown

> A brief introduction to markdown.

## Summary

Use the `>` to start a summary section. See the summary above.

```
> A brief introduction to markdown.
```

## Sections

Sections on different levels are prepended with at least one `#`, which corresponds to a `<h1></h1>` and `##` corresponds to  `<h2></h2>` etc.

Example:

```
# Title

> A brief introduction to markdown.

## Summary

Use the `>` to start a summary section. See the summary above.
```

Result: See top of this document.

## Lists

### Bulleted

Use `*` or `-` for each item in the list.

```
* First item
* Second item
* Third item
```

Result:

* First item
* Second item
* Third item

### Numbered

Use numbers `1.`, `2.` for each item in the list. You can also just use `1.` for all items, which is useful if you later want to re-order items without having to adjust the numbers as well.

```
1. First item
1. Second item
1. Third item
```

Result:

1. First item
1. Second item
1. Third item

### Checked lists

Use `- [ ]` for each unchecked item in the list and `- [x]` for each checked item.

```
- [ ] First item
- [ ] Second item
- [x] Third item
```

Result:

- [ ] First item
- [ ] Second item
- [x] Third item

## Formatting

`**Bold**` -> **Bold**

`_Italic_` -> _Italic_

`**_Bold and Italic_**` -> **_Bold and Italic_**

## Links

You add links by using square brackets and parenthesis like `[link text](linkurl)`

```
[Link to docs site](https://docs.linkping.org)
```

Result:

[Link to docs site](https://docs.linkping.org)

## Images

Images are basically links but with a `!` prepended to it. Note that the url can be a relative path from the current document. You can also add a picture label at the end which is shown when the mouse is hovering above the image.

```
![A cat](cat.jpeg "Meow!")
```

![A cat](cat.jpeg "Meow!")

## Linked images

You can combine links with images by inserting the image markdown into the link alt text field (the first brackets).

```
[![A cat](cat.jpeg "Click for more!")](https://duckduckgo.com/?t=ffab&q=cats&iax=images&ia=images)
```

[![A cat](cat.jpeg "Click for more!")](https://duckduckgo.com/?t=ffab&q=cats&iax=images&ia=images)

## Code or unmarked text

Use triple backtics \`\`\` before and after the text to display it as is. This is used in this document to show what the markdown looks like.

```
const variable = true
```

## Tables

The first row in a table is the table header. The second row marks the beginning of the table and the following rows are the rows of the table.

```
| Color | Färg | Coleur |
|---|---|---|
| Black | Svart | Noir |
| Brown | Brun | Brun |
| Red | Röd | Rouge |
| Orange | Orange | Orange |
| Yellow | Gul | Jaune |
| Green | Grön | Vert |
| Blue | Blå | Bleu |
| Violet | Violett | Violet |
| Grey | Grå | Grise |
| White | Vit | Blanche |
| Gold | Guld | Or |
| Silver | Silver | Argent |
```

Result:

| Color | Färg | Coleur |
|---|---|---|
| Black | Svart | Noir |
| Brown | Brun | Brun |
| Red | Röd | Rouge |
| Orange | Orange | Orange |
| Yellow | Gul | Jaune |
| Green | Grön | Vert |
| Blue | Blå | Bleu |
| Violet | Violett | Violet |
| Grey | Grå | Grise |
| White | Vit | Blanche |
| Gold | Guld | Or |
| Silver | Silver | Argent |

## Comments

Can be done using html comments using the `<!-- ... -->` syntax.

```
Some text with <!-- this will not be visible --> a comment inside.
```

Result:

Some text with <!-- this will not be visible --> a comment inside.

