# Electronics

> Brief notes and explanations to different concepts within electronics. Mostly taken from Wikipedia to gather "stubs" of information.

## Laws and Mathematics

* [`Ohm's Law`](https://en.wikipedia.org/wiki/Ohm%27s_law)
* [`Kirchoff's Circuit Laws`](https://en.wikipedia.org/wiki/Kirchhoff%27s_circuit_laws)

## Components

Rough descriptions of some types of components. See resources below for a more comprehensive categorization where you can search arts by their number or keywords.

Resources:

* [`Octopart`](https://octopart.com/)  component database and plugin to KiCad
* [`SnapEDA`](https://www.snapeda.com/) component database and [plugin](https://www.snapeda.com/plugins/) to KiCad

### Batteries

You can measure the battery voltage using a multimeter, but the battery might still be dead even if the voltage looks fine. The best way to see if a battery is dead or not is to measure it while connected to a load, e.g. a small 100 ohms resistor. If there's a small voltage drop, the battery is fine, otherwise it's dead. For example if you measure 1.5V without a load and 0.8V with a load.

### Capacitors

### Cables and Wire

### Connectors

### Diods

A diod is a semiconductor and acts like a valve in a circuit, i.e. it only allows the flow of current in one direction. The anode is a p-doped material and the cathode is a n-doped material and they meet in what's called a pn-junction to form a depletion region, where some electrons from the cathod moves over to the anode and some holes from the anode moves over to the cathod. This creates a barrier with a potential difference of ~0.7V, preventing more electrons from flowing.

In a DC circuit, the anode must be connected to the positive and the cathode to the negative in order for current to flow. The voltage over the diod must exceed the potential difference of the barrier (forward potential barrier) to open. Diods also creates a voltage drop.

If the diod is reversed then the holes in the anode will move towards the negative and the electrons towards the positive, increasing the barrier. This can be broken if the voltage exceeds what's called the max reverse bias voltage. This will break the diod and the current will flow in the opposite direction. Imagine a valve in a pipe where the water pressure is just too high, eventually the valve will break.

You can use a diod to protect circuits, if e.g. the power supply was connected back to front, allowing current to pass by the diod instead of through the circuit. A concrete example is a so called flywheel diod, which can be used with a relay. If the relay is connected to a LED, it can damaged when the relay is turned off since the coil is an inductor that stores energy it can produce a spike of current through the LED and damage it. If you connect a diod inbetween the LED and the relay (in parallel) the diod will provide a path for the coil to dissipate its energy safely, protecting the LED.

In AC circuits there's no purely positive or negative side so sometimes a diod will allow current to pass through and sometimes it will prevent current to flow when the voltage changes from positive to negative. This is used in rectifiers, see below.

You can use a multimeter to test a diod. Connecting the black probe to the cathode and the red probe to the anode and selecting the diod setting on the multimeter. This will show the minimum voltage needed for the diod to open. You can also turn the diod around to get `OL` (outside limits) showing on the multimeter. This way we know that the diod is not broken and is preventing current from flowing.

#### 'Normal' Diod

Examples: `1N4001`, `1N4004`, `1N4007`, `1N5408`.

#### LED

Describe briefly what a LED is, the different voltages and how to calculate the size of the resistor.

#### Zener Diod

#### Schottky Diod

### Inductors

### Resistors

### Relays

Relays are used to turn a secondary circuit on or off. Usually the primary circuit is using lower power and the secondary higher power. Relays can be normally open or normally closed. This is the state the relay is in when it's not powered on.

A normally opened relay can be used for e.g. fan cooling. So on the primary side you can have some temperature sensor (e.g. a bimetallic strip than bends due to changes in temperature, thus closing a circuit) that in turn triggers the relay, which then causes the fan to cool until the temperature goes down which then causes the fan to stop.

A normally closed relay can be used for e.g. a water pump system to maintain a certain level of water in a storage tank. When the water level is low, the pump is on, but when it reaches the level we require, it completes the primary circuit, which cuts the power to the pump.

A third type of relays is the latching relay, which has a positional memory. Once activated, they will remain in their last position without the need for any further input or current. It uses two different electromagnets to pull a piston between two different positions. The first electromagnet pulls the piston to the open position which then remains in that position until the second electromagnet pulls the piston back into the closed position. A use case for this is in elevators. Say you want to go up, you push the up-button on the elevator control, which pulls the piston to the open position, lighting a LED inside the button. The LED stays on, until the elevator has arrived to your position, which then triggers the second electromagnet to pull the piston into the off position, turning the LED off.

Like any switch a relay can have multiple poles which allows more than one secondary circuit to be energized. Naturally they can also be single or double throw. A double throw relay is also called a changeover relay. Obviously there are also combinations of this, e.g. a double pole double throw (DPDT) can be used to control two states in two secondary circuits.

#### Electromechanical Relays

Uses an electromagnet in the primary circuit to generate a magnetic field to pull a lever that in turn opens or closes the secondary circuit.

#### Solid State Relays

Similar to electromechanical relays but has no moving parts. Instead uses electric and optical properties of semiconductors. Can for example be constructed by a LED (corresponds to the electromagnet) that provides optical coupling by shining a beam of light across a gap and into the receiver of an adjacent photo sensitive transistor (phototransistor).

### Sensors

### Switches and Buttons

### Transformers

### [`Transistors`](https://en.wikipedia.org/wiki/Transistor)

A transistor is a semiconductor device used to amplify or switch electronic signals and electrical power. Transistors are one of the basic building blocks of modern electronics. It is composed of semiconductor material usually with at least three terminals for connection to an external circuit. A voltage or current applied to one pair of the transistor's terminals controls the current through another pair of terminals. Because the controlled (output) power can be higher than the controlling (input) power, a transistor can amplify a signal. Today, some transistors are packaged individually, but many more are found embedded in integrated circuits.

#### [`Bipolar Junction Transistor`](https://en.wikipedia.org/wiki/Bipolar_junction_transistor)

A bipolar junction transistor (BJT) is a type of transistor that uses both electrons and electron holes as charge carriers. In contrast, a unipolar transistor, such as a field-effect transistor, uses only one kind of charge carrier.

#### [`MOSFET`](https://en.wikipedia.org/wiki/MOSFET)

The metal–oxide–semiconductor field-effect transistor (MOSFET, MOS-FET, or MOS FET), also known as the metal–oxide–silicon transistor (MOS transistor, or MOS), is a type of insulated-gate field-effect transistor that is fabricated by the controlled oxidation of a semiconductor, typically silicon.

* [FQP30N06L](https://cdn.sparkfun.com/datasheets/Components/General/FQP30N06L.pdf)

## Integrated Circuits

### [`Operational Amplifier`](https://en.wikipedia.org/wiki/Operational_amplifier)

### [`555`](https://en.wikipedia.org/wiki/555_timer_IC)

One of the most popular chips of all time, constructed by Hans Camenzind. Over a billion units are manufactured annually. It can run in different modes and solves many different use cases depending on said mode.

## Circuit Patterns

### Voltage Divider

### Pull-up/Pull-down resistors

### [`RC circuit`](https://en.wikipedia.org/wiki/RC_circuit)

A resistor–capacitor circuit (RC circuit), or RC filter or RC network, is an electric circuit composed of resistors and capacitors.

### [`RL circuit`](https://en.wikipedia.org/wiki/RL_circuit)

A resistor–inductor circuit (RL circuit), or RL filter or RL network, is an electric circuit composed of resistors and inductors driven by a voltage or current source.

### [`LC circuit`](https://en.wikipedia.org/wiki/LC_circuit)

An LC circuit, also called a resonant circuit, tank circuit, or tuned circuit, is an electric circuit consisting of an inductor, represented by the letter L, and a capacitor, represented by the letter C, connected together. The circuit can act as an electrical resonator, an electrical analogue of a tuning fork, storing energy oscillating at the circuit's resonant frequency.

### [`RLC circuit`](https://en.wikipedia.org/wiki/RLC_circuit)

An RLC circuit is an electrical circuit consisting of a resistor (R), an inductor (L), and a capacitor (C), connected in series or in parallel. The name of the circuit is derived from the letters that are used to denote the constituent components of this circuit, where the sequence of the components may vary from RLC.

### [`H-bridge`](https://en.wikipedia.org/wiki/H-bridge)

### Inverters

### Rectifiers

#### Half Way Rectifier

One diod used to allow the positive voltage/current to pass and preventing it to flow otherwise, causing the resulting output as a half wave `_/\__/\_`.

#### Full Bridge Rectifier

Like two half way rectifiers (but using four diods) also converting the negative side to positive `/\/\/\`. Adding a capacitor to the end also smooths the voltage out.

### Band Pass Filter

TODO: Move into R,L,C circuits?

### Low Pass Filter

TODO: Move into R,L,C circuits?

### High Pass Filter

TODO: Move into R,L,C circuits?

### CMOS inverter

### Amplifiers

### [`Buck converter`](https://en.wikipedia.org/wiki/Buck_converter)

A buck converter (step-down converter) is a DC-to-DC power converter which steps down voltage (while drawing less average current) from its input (supply) to its output (load). It is a class of switched-mode power supply (SMPS) typically containing at least two semiconductors (a diode and a transistor, although modern buck converters frequently replace the diode with a second transistor used for synchronous rectification) and at least one energy storage element, a capacitor, inductor, or the two in combination. To reduce voltage ripple, filters made of capacitors (sometimes in combination with inductors) are normally added to such a converter's output (load-side filter) and input (supply-side filter).

## Tools and Equipment

### [`Multimeter`](https://en.wikipedia.org/wiki/Multimeter)

A multimeter is a measuring instrument that can measure multiple electrical properties. A typical multimeter can measure voltage, resistance, and current, in which case it is also known as a volt-ohm-milliammeter (VOM).

### [`Soldering iron`](https://en.wikipedia.org/wiki/Soldering_iron)

A soldering iron is a hand tool used in soldering. It supplies heat to melt solder so that it can flow into the joint between two workpieces.

### [`Oscilloscope`](https://en.wikipedia.org/wiki/Oscilloscope)

An oscilloscope, previously called an oscillograph, and informally known as a scope or o-scope, CRO (for cathode-ray oscilloscope), or DSO (for the more modern digital storage oscilloscope), is a type of electronic test instrument that graphically displays varying signal voltages, usually as a calibrated two-dimensional plot of one or more signals as a function of time.

The displayed waveform can then be analyzed for properties such as amplitude, frequency, rise time, time interval, distortion, and others. Originally, calculation of these values required manually measuring the waveform against the scales built into the screen of the instrument. Modern digital instruments may calculate and display these properties directly.

### [`Signal generator`](https://en.wikipedia.org/wiki/Signal_generator)

A signal generator is one of a class of electronic devices that generates electronic signals with set properties of amplitude, frequency, and wave shape. These generated signals are used as a stimulus for electronic measurements, typically used in designing, testing, troubleshooting, and repairing electronic or electroacoustic devices, though it often has artistic uses as well.

## Microcontrollers

### [`Arduino`](https://en.wikipedia.org/wiki/Arduino)

Arduino is an open-source hardware and software company, project and user community that designs and manufactures single-board microcontrollers and microcontroller kits for building digital devices. Its hardware products are licensed under a CC-BY-SA license, while software is licensed under the GNU Lesser General Public License (LGPL) or the GNU General Public License (GPL), permitting the manufacture of Arduino boards and software distribution by anyone. Arduino boards are available commercially from the official website or through authorized distributors.

### [`ATtiny`](https://en.wikipedia.org/wiki/ATtiny_microcontroller_comparison_chart)

ATtiny (also known as TinyAVR) are a subfamily of the popular 8-bit AVR microcontrollers, which typically has fewer features, fewer I/O pins, and less memory than other AVR series chips.

### [`ESP32`](https://en.wikipedia.org/wiki/ESP32)

ESP32 is a series of low-cost, low-power system on a chip microcontrollers with integrated Wi-Fi and dual-mode Bluetooth. The ESP32 series employs either a Tensilica Xtensa LX6 microprocessor in both dual-core and single-core variations or a single-core RISC-V microprocessor and includes built-in antenna switches, RF balun, power amplifier, low-noise receive amplifier, filters, and power-management modules. ESP32 is created and developed by Espressif Systems, a Shanghai-based Chinese company, and is manufactured by TSMC using their 40 nm process. It is a successor to the ESP8266 microcontroller.

### [`ESP8266`](https://en.wikipedia.org/wiki/ESP8266)

The ESP8266 is a low-cost Wi-Fi microchip, with a full TCP/IP stack and microcontroller capability, produced by Espressif Systems in Shanghai, China.

### [`STM32`](https://en.wikipedia.org/wiki/STM32)

STM32 is a family of 32-bit microcontroller integrated circuits by STMicroelectronics. The STM32 chips are grouped into related series that are based around the same 32-bit ARM processor core, such as the Cortex-M33F, Cortex-M7F, Cortex-M4F, Cortex-M3, Cortex-M0+, or Cortex-M0. Internally, each microcontroller consists of the processor core, static RAM, flash memory, debugging interface, and various peripherals.

## Techniques and Practices

### Soldering

### Circuit Design

#### Software

* [`KiCad`](https://www.kicad.org/) free software
* [`EasyEDA`](https://easyeda.com/) online tool, free and paid plans

## Lingo

* `Anode` The end of a component where conventional current flows into
* `BOM` Bill of Materials
* `Cathode` The end of a component where conventional current flows out from. Mnemonic: `CCD` _Cathode Current Departs_. Note that this is the _positive_ side of a battery. On a LED this is the leg connected to the negative, which can be a bit confusing
* `DRC` Design Rule Checking includes rules that stand for defining the minimum spacing between components for the entire circuit board or for individual layers.
* `EDA` Electronic Design Automation
* `EGS` Electronic Grade Silicon - wafer material
* `floating` Something is floating if it isn't connected to anything, e.g. a pin on an Arduino. The pin would essentially be working as an antenna, producing random current, thus causing the Arduino to read random voltages that can be interpreted as high or low. See pull-up and pull-down resistors
* `FPGA` Field Programmable Gate Array
* `IC` Integrated Circuit
* `MCU` Microcontroller Unit
* `MRP` Material Requirements Planning is a system that helps manufacturers plan, schedule and manage their inventory during the manufacturing process
* `PCB` Printed Curcuit Board
* `PSU` Power Supply Unit
* `PWM` Pulse Width Modulation. Basically a square wave where you keep the frequency fixed and change the duty cycle, e.g. the time period when the current is on

## Questions/TODO

* How to read a datasheet? What to focus on? What are all these diagrams saying?
* What are some examples of important components and ICs to know about? Like the ABC of components, e.g. the 555 etc. Create a list of commonly known and important specific components.
* What are the differences between PNP- and NPN-transistor? When do you want the PNP rather than that NPN?
