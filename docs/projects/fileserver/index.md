# Create a local fileserver
## Connect to wifi using the terminal
Find wifi card using
```bash
ifconfig
```
mine is called wlp3s0. Create the file
```bash
/etc/wpa_supplicant.conf
```
with the contents
```bash
network={
    ssid=[the_network_name]
    psk=[the_password]
}
```
Then run the commands
```bash
sudo wpa_supplicant -B -i wlp3s0 -c /etc/wpa_supplicant.conf -D nl80211
sudo dhclient wlp3s0
```
In case this doesn't work you may try
```bash
sudo killall wpa_supplicant
```
and then try the above again.


## Connect to the pi via ssh
To show my local ip address
```bash
ifconfig
```

To show all devices in local network (pi has name 'Nisca corporation')
```bash
sudo arp-scan -l
```

Connect to pi via ssh (password: raspberry)
```bash
ssh -Y pi@[ip-address]
```

## Install and configure samba on the pi
Now that we are connected to the pi lets install samba:
```bash
sudo apt-get install samba samba-common-bin
```

To setup samba we first need to create a folder where our shares go
```bash
sudo mkdir -m 1777 /share
```
We configure samba by appending
```bash
[myshare]
path = /share
browseable=Yes
writeable=Yes
create mask=0777
directory mask=0777
public=yes
```
to
```bash
/etc/samba/smb.conf
```

Next we add a samba-user to the share (and password)
```bash
sudo smbpasswd -a pi
```
The last step on this section is to load these changes
```bash
sudo systemctl restart smbd
```

## Client side configuration of the fileserver

Now we are done with setting up the share on the raspberry.
Next we will connect to the share from another computer running GNU/Linux.
This is simply done by the following command
```bash
sudo mount -t cifs //[ip-address]/share -o username=[smb-username] /media/samba
```

This command has to be run every time we restart our computer. To avoid this we may
modify /etc/fstab by appending
```bash
//[ip-address]/share /media/samba cifs credentials=/etc/samba/user.cred 0 0
```
The file /etc/samba/user.cred does not exist so we create it with the content
```bash
username=[smb-username]
password=[smb-password]
```
