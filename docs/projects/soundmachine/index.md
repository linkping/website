#Sound Machine
Here will come a log of the sound machine project.
## The Oscillator
![Oscillator schematics](oscillator.jpg)

In steps:
Step 1: the voltage is connected. Current starts flowing. The entire voltage drop is over the 2 resistors - the capacitor has equal charges on both sides and no potential drop. The voltage drop over the resistors is R*I.

Step 2: the capacitor starts filling up - the current decreases. The voltage drop is shared between the resistors and the capacitor, where electrons are bustling and wanting to get over to the other side.

Step 3: the voltage over the capacitor, which is the same as the voltage over the transistor + diode, reaches the voltage value for avalanche breakdown of the transistor - the electrons thunder through the transistor and the capacitor once again has equal charges on both sides and no potential drop.

Here is a picture of the outputsignal on an oscilloscope.
![Scope output](scope.jpeg)

Our outputsignal is over the diode. An led usually needs a few volts to light up. The outputsignal we get over the transistor is a sawtooth pattern varying about 10 millivolts from top to bottom.
The instantaneous rise is assumed to be the avalanche, where the voltage drop over diode + transistor switches to only diode. [Kerry Wong](http://www.kerrywong.com/2013/05/18/avalanche-pulse-generator-build-using-2n3904/) writes: 'the capacitor is only charged briefly to a fraction of the supply voltage before avalanche breakdown occurs'. When the breakdown happens the voltage drop 12V minus the drop over the two inital resistors is only over the diode, so therefore the spike. As the capacitor discharges the voltage drops. The discharge is not as instantaneous as one might expect given the term 'avalanche', but this is consistent with Wongs results ([different paper](http://www.kerrywong.com/2014/03/19/bjt-in-reverse-avalanche-mode/)).



Question: According to Wong a voltage drop of 100 V is necessary in order to trigger an avalanche - why does our oscillator (and lookmumnocomputers) work with 12V?
Qustion: Is the variation in millivolts happening above a baseline that is a few volts? IE is the voltage over the transistor just under the threshold to begin with? This seems consistent with measurements - the voltage
when measured with a multimeter is on the order of 1 V. 