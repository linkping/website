# Create Zines using LaTeX
##A5 and A6 booklets
This is how I did it - modern printers sometimes have a feature for creating booklets and of course that may be much easier. But if you have to do it yourself here are the two methods I used.
###A5
The simplest here is to install the package [pdfbook2](https://github.com/jenom/pdfbook2) (included in texlive-extra-utils if you use ubuntu, which can be installed via sudo apt-get).
Then just run pdfbook2 your_regular_file_formatted_as_a4_article_for_example.pdf and it outputs a booklet.

###A6
Printing is quite expensive if you don't have your own printer, so if you want to batch print on a small budget this solution, provided by Malipivo in [this bloggpost](https://tex.stackexchange.com/questions/179119/how-to-automatically-output-page-order-to-print-8-pages-of-a-booklet-on-a-single).
Even without understanding the cool script, you can use it as Mailipivo suggests:
pdflatex mal-arrange-0.tex (just copy and insert your own text in mal-arrange-0.tex instead of the huge number 1).  
pdflatex mal-arrange-1.tex (mal-arrange-1.tex uses mal-arrange-0.pdf)  
pdflatex mal-arrange-2.tex (mal-arrange-2.tex uses mal-arrange-1.pdf)  
pdflatex mal-arrange-3.tex (mal-arrange-3.tex uses mal-arrange-2.pdf)  
pdflatex mal-arrange-4.tex (mal-arrange-4.tex uses mal-arrange-1.pdf)   

The last one will give you 4 pages per A4-page - i.e. 8 pages per A4-page since you print on two sides. The pages will also be in the correct order for stapling together as a booklet. 

![Here you should see an a4 split into 4 pages](example_a6.png)



## Songbook
*Motivation: Its cumbersome scrolling on screens when you're playing guitar and singing. Got suggestions for songs from Filwe and his brother who is learning to play and added some songs about around Linköping and some that I just like.*

The [songs](http://songs.sourceforge.net/) package, copyrighted by Kevin Hamlen and released under the GPL, is a great tool. Its really easy to make chorded songs - you just write *\[C]Kazam* and the C chord goes nicely on top of the word *Kazam*.

It even remembers the chord progression, so once you've typed out one verse:
```
\beginverse
\[C]Du är det \[F]finas\[G]te jag \[C]vet\[F].\[G]
\[C]Du är det d\[F]yras\[Dm]te i v\[G]ärlden.
\[Dm]Du är som stj\[G]ärnorna, som v\[C]indarna, som v\[Am7]ågorna,
som f\[F]åglarna, som bl\[Dm]ommorna p\[G]å marken.
\endverse
```
the next time you have a verse with the same chord pattern you can just do:
```
\beginverse
^Du är min ^ledstjärna ^och ^vän.
^Du är min ^tro, mitt ^hopp, min ^kärlek.
^Du är mitt ^blod och mina ^lungor, mina ^ögon,
mina ^skuldror, mina ^händer och mitt ^hjärta.
\endverse
```
and the hats insert the correct chord! 

##Booklet about "Things you can do two hours before you go to bed without screens"
This was kind of self-therapy, writing this one. It has some sudoku in it, for which the [sudokubundle](https://www.ctan.org/pkg/sudokubundle) package was very useful (the specific sudoku puzzles come from the collection there, but it can also generate new puzzles - and solve them, actually), and some mandalas to colour - mostly from images created by graphical artists, but also some created directly in LaTeX by the participants in [this LaTeX competition](https://tex.stackexchange.com/questions/496414/tex-art-fun-mandalas) - and also some other things.

![This should be a picture of my zines](zines.jpg)
[You can find tex and pdf-files here.](https://github.com/gnyrfta/zines)





