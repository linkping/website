# Mini Arcade
This is a mini arcade where you can play Pong, a simplified version of the classic game Asteroids, and an etch-a-sketch. It also controls a strip of neopixels - you can set the red, green and blue values and turn it off and on.
It also ran a program that displayed the mandelbrot set, but the Arduino got maxed out, so something had to be removed.


##Code and hardware info
[Mini Arcade on Codeberg](https://codeberg.org/gnyrfta/gameboy)
The project contains a fair amount of spaghetti code. The part of the code which could be useful another time I think is the part that lets you use the joystick and the lcd screen as an interactive menu. 


<!-- blank line -->
<!--    <source src="https://www.gnyrftacode.se/mini_arcade_1.mp4" type="video/mp4">-->
<!--    <source src="https://www.gnyrftacode.se/mini_arcade_2.mp4" type="video/mp4">-->

