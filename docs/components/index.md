# Transistors

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/40673.pdf">40673</a></td>
<td>Dual gate, MOSFET</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=141">2G1025</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n396.pdf">2N396A</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n404.pdf">2N404</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n491.pdf">2N491</a></td>
<td>NPN, unijunction</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n525.pdf">2N525</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n526.pdf">2N526</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n706.pdf">2N706</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n708.pdf">2N708</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n914.pdf">2N914</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n918.pdf">2N918</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1132.pdf">2N1132</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1304.pdf">2N1304</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1308.pdf">2N1308</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1309.pdf">2N1309</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1613.pdf">2N1613</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1671.pdf">2N1671</a></td>
<td>PN, unijunction</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1711.pdf">2N1711</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=1230">2N1721</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n1893.pdf">2N1893</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=1439">2N1970</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2160.pdf">2N2160</a></td>
<td>PN, unijunction</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2218.pdf">2N2218</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2219.pdf">2N2219</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2222.pdf">2N2222</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2324.pdf">2N2324</a></td>
<td>Silicon controlled rectifier</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2369.pdf">2N2369</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2646.pdf">2N2646</a></td>
<td>PN, unijunction</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2904.pdf">2N2904</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n2905.pdf">2N2905</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3053.pdf">2N3053</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3055.pdf">2N3055</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3070.pdf">2N3070</a></td>
<td>N-channel, FET</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3235.pdf">2N3235</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3250.pdf">2N3250</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3494.pdf">2N3494</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3702.pdf">2N3702</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3704.pdf">2N3704</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3716.pdf">2N3716</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3772.pdf">2N3772</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3866.pdf">2N3866</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n3904.pdf">2N3904</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4140.pdf">2N4140</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4141.pdf">2N4141</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=3963">2N4142</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=3964">2N4143</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4227.pdf">2N4227</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=3989">2N4228</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4302.pdf">2N4302</a></td>
<td>N-channel, JFET</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4402.pdf">2N4402</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4852.pdf">2N4852</a></td>
<td>PN, unijunction</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n4918.pdf">2N4918</a></td>
<td>PNP, medium power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5294.pdf">2N5294</a></td>
<td>NPN, medium power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5400.pdf">2N5400</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5459.pdf">2N5459</a></td>
<td>N-channel, general purpose amplifier</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5461.pdf">2N5461</a></td>
<td>P-channel, JFET</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5494.pdf">2N5494</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5631.pdf">2N5631</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5683.pdf">2N5683</a></td>
<td>PNP, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5685.pdf">2N5685</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n5771.pdf">2N5771</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2n6543.pdf">2N6543</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/2s301.pdf">2S301</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ac126.pdf">AC126</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ac128.pdf">AC128</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ac151.pdf">AC151</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ad161.pdf">AD161</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ad162.pdf">AD162</a></td>
<td>NPN, high power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/af124.pdf">AF124</a></td>
<td>PNP, high frequency</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/asz16.pdf">ASZ16</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc107.pdf">BC107B</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc108.pdf">BC108B</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc109.pdf">BC109C</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc141-16.pdf">BC141-16</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc148c.pdf">BC148C</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc161-16.pdf">BC161-16</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc167.pdf">BC167</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc177.pdf">BC177B</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc178.pdf">BC178B</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=22448">BC181</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc182l.pdf">BC182L</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc184.pdf">BC184C</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc212.pdf">BC212</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc238.pdf">BC238</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc257.pdf">BC257</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc327.pdf">BC327</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc337.pdf">BC337</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bc547b.pdf">BC547B</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bd136.pdf">BD136</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bd437.pdf">BD437</a></td>
<td>NPN, medium power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bd439.pdf">BD439</a></td>
<td>NPN, medium power</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bd647.pdf">BD647</a></td>
<td>NPN, darlington</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bd650.pdf">BD650</a></td>
<td>NPN, darlington</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bf115.pdf">BF115</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bf179.pdf">BF179A</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=26640">BF195</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bfy50.pdf">BFY50</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bfy50.pdf">BFY51</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/bsw66.pdf">BSW66</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=34816">FT2955</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ft50.pdf">FT50</a></td>
<td>N-channel, MOSFET</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mje2955.pdf">MJE2955</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mje2955.pdf">MJE3055</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=41799">MP3730</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mps2369.pdf">MPS2369</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mps3646.pdf">MPS3646</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="https://alltransistors.com/transistor.php?transistor=42507">MPSL08</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mpsu45.pdf">MPSU45</a></td>
<td>NPN, darlington</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mpsu56.pdf">MPSU56</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tf78-30.pdf">TF78/30</a></td>
<td>PNP</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tip29c.pdf">TIP29C</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tip3055.pdf">TIP3055</a></td>
<td>NPN</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/u1898.pdf">U1898</a></td>
<td>N-channel, JFET</td>
</tr>

</tbody>
</table>

# 7400-series

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor" id="7400">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7400.pdf">7400</a></td>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="7401">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7401.pdf">7401</a></td>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="7402">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7402.pdf">7402</a>
<td>Quad 2-input NOR gate</td>
</td></tr>

<tr class="anchor" id="7403">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7403.pdf">7403</a>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="7404">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7404.pdf">7404</a>
<td>Hex inverter gate</td>
</tr>

<tr class="anchor" id="7405">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7405.pdf">7405</a>
<td>Hex inverter gate</td>
</tr>

<tr class="anchor" id="7406">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7406.pdf">7406</a>
<td>Hex inverter gate</td>
</tr>

<tr class="anchor" id="7407">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7407.pdf">7407</a>
<td>Hex buffer gate</td>
</tr>

<tr class="anchor" id="7408">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7408.pdf">7408</a>
<td>Quad 2-input AND gate</td>
</tr>

<tr class="anchor" id="7409">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7409.pdf">7409</a>
<td>Quad 2-input AND gate</td>
</tr>

<tr class="anchor" id="7410">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7410.pdf">7410</a>
<td>Triple 3-input NAND gate</td>
</tr>

<tr class="anchor" id="7411">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7411.pdf">7411</a>
<td>Triple 3-input AND gate</td>
</tr>

<tr class="anchor" id="7412">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7412.pdf">7412</a>
<td>Triple 3-input NAND gate</td>
</tr>

<tr class="anchor" id="7413">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7413.pdf">7413</a>
<td>Dual 4-input NAND gate</td>
</tr>

<tr class="anchor" id="7414">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7414.pdf">7414</a>
<td>Hex inverter gate</td>
</tr>

<tr class="anchor" id="7415">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7415.pdf">7415</a>
<td>Triple 3-input AND gate</td>
</tr>

<tr class="anchor" id="7416">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7416.pdf">7416</a>
<td>Hex inverter gate</td>
</tr>

<tr class="anchor" id="7420">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7420.pdf">7420</a>
<td>Dual 4-input NAND gate</td>
</tr>

<tr class="anchor" id="7421">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7421.pdf">7421</a>
<td>Dual 4-input AND gate</td>
</tr>

<tr class="anchor" id="7426">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7426.pdf">7426</a>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="7427">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7427.pdf">7427</a>
<td>Triple 3-input NOR gate</td>
</tr>

<tr class="anchor" id="7430">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7430.pdf">7430</a>
<td>Single 8-input NAND gate</td>
</tr>

<tr class="anchor" id="7432">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7432.pdf">7432</a>
<td>Quad 2-input OR gate</td>
</tr>

<tr class="anchor" id="7438">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7438.pdf">7438</a>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="7440">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7440.pdf">7440</a>
<td>Dual 4-input NAND gate</td>
</tr>

<tr class="anchor" id="7442">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7442.pdf">7442</a>
<td>BCD to decimal decoder</td>
</tr>

<tr class="anchor" id="7445">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7445.pdf">7445</a>
<td>BCD to decimal decoder/driver</td>
</tr>

<tr class="anchor" id="7447">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7447.pdf">7447</a>
<td>BCD to 7-segment decoder/driver</td>
</tr>

<tr class="anchor" id="7448">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7448.pdf">7448</a>
<td>BCD to 7-segment decoder/driver</td>
</tr>

<tr class="anchor" id="7472">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7472.pdf">7472</a>
<td>AND gated J-K master-slave flip-flop, asynchronous preset and clear</td>
</tr>

<tr class="anchor" id="7474">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7474.pdf">7474</a>
<td>Dual D positive edge triggered flip-flop, asynchronous preset and clear</td>
</tr>

<tr class="anchor" id="7476">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7476.pdf">7476</a>
<td>Dual J-K flip-flop, asynchronous preset and clear</td>
</tr>

<tr class="anchor" id="7485">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7485.pdf">7485</a>
<td>4-bit magnitude comparator</td>
</tr>

<tr class="anchor" id="7486">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7486.pdf">7486</a>
<td>Quad 2-input XOR gate</td>
</tr>

<tr class="anchor" id="7490">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7490.pdf">7490</a>
<td>Decade counter (separate divide-by-2 and divide-by-5 sections)</td>
</tr>

<tr class="anchor" id="7493">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/7493.pdf">7493</a>
<td>4-bit binary counter (separate divide-by-2 and divide-by-8 sections)</td>
</tr>

<tr class="anchor" id="74107">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74107.pdf">74107</a>
<td>Dual J-K flip-flop, clear</td>
</tr>

<tr class="anchor" id="74109">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74109.pdf">74109</a>
<td>Dual J-NotK positive-edge-triggered flip-flop, clear and preset</td>
</tr>

<tr class="anchor" id="74112">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74112.pdf">74112</a>
<td>Dual J-K negative-edge-triggered flip-flop, clear and preset</td>
</tr>

<tr class="anchor" id="74121">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74121.pdf">74121</a>
<td>Monostable multivibrator</td>
</tr>

<tr class="anchor" id="74123">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74123.pdf">74123</a>
<td>Dual retriggerable monostable multivibrator, clear</td>
</tr>

<tr class="anchor" id="74125">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74125.pdf">74125</a>
<td>Quad bus buffer, negative enable</td>
</tr>

<tr class="anchor" id="74132">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74132.pdf">74132</a>
<td>Quad 2-input NAND gate</td>
</tr>

<tr class="anchor" id="74133">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74133.pdf">74133</a>
<td>Single 13-input NAND gate</td>
</tr>

<tr class="anchor" id="74138">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74138.pdf">74138</a>
<td>3-to-8 line decoder/demultiplexer, inverting outputs</td>
</tr>

<tr class="anchor" id="74139">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74139.pdf">74139</a>
<td>Dual 2-to-4 line decoder/demultiplexer, inverting outputs</td>
</tr>

<tr class="anchor" id="74148">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74148.pdf">74148</a>
<td>8-line to 3-line priority encoder</td>
</tr>

<tr class="anchor" id="74150">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74150.pdf">74150</a>
<td>16-line to 1-line data selector/multiplexer</td>
</tr>

<tr class="anchor" id="74151">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74151.pdf">74151</a>
<td>8-line to 1-line data selector/multiplexer</td>
</tr>

<tr class="anchor" id="74153">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74153.pdf">74153</a>
<td>Dual 4-line to 1-line data selector/multiplexer, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74154">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74154.pdf">74154</a>
<td>4-to-16 line decoder/demultiplexer, inverting outputs</td>
</tr>

<tr class="anchor" id="74160">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74160.pdf">74160</a>
<td>Synchronous presettable 4-bit decade counter, asynchronous clear</td>
</tr>

<tr class="anchor" id="74161">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74161.pdf">74161</a>
<td>Synchronous presettable 4-bit binary counter, asynchronous clear</td>
</tr>

<tr class="anchor" id="74163">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74163.pdf">74163</a>
<td>Synchronous presettable 4-bit binary counter, synchronous clear</td>
</tr>

<tr class="anchor" id="74165">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74165.pdf">74165</a>
<td>8-bit parallel-in serial-out shift register, parallel load, complementary outputs</td>
</tr>

<tr class="anchor" id="74172">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74172.pdf">74172</a>
<td>16-Bit Multiple-Port Register File with 3-State Outputs</td>
</tr>

<tr class="anchor" id="74173">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74173.pdf">74173</a>
<td>Quad D flip-flop, asynchronous clear</td>
</tr>

<tr class="anchor" id="74174">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74174.pdf">74174</a>
<td>Hex D flip-flop, common asynchronous clear</td>
</tr>

<tr class="anchor" id="74175">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74175.pdf">74175</a>
<td>Quad D edge-triggered flip-flop, complementary outputs and asynchronous clear</td>
</tr>

<tr class="anchor" id="74190">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74190.pdf">74190</a>
<td>Synchronous presettable up/down 4-bit decade counter</td>
</tr>

<tr class="anchor" id="74191">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74191.pdf">74191</a>
<td>Synchronous presettable up/down 4-bit binary counter</td>
</tr>

<tr class="anchor" id="74192">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74192.pdf">74192</a>
<td>Synchronous presettable up/down 4-bit decade counter, clear</td>
</tr>

<tr class="anchor" id="74193">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74193.pdf">74193</a>
<td>Synchronous presettable up/down 4-bit binary counter, clear</td>
</tr>

<tr class="anchor" id="74194">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74194.pdf">74194</a>
<td>4-bit bidirectional universal shift register</td>
</tr>

<tr class="anchor" id="74196">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74196.pdf">74196</a>
<td>Presettable 4-bit decade counter/latch</td>
</tr>

<tr class="anchor" id="74197">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74197.pdf">74197</a>
<td>Presettable 4-bit binary counter/latch</td>
</tr>

<tr class="anchor" id="74221">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74221.pdf">74221</a>
<td>Dual monostable multivibrator</td>
</tr>

<tr class="anchor" id="74240">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74240.pdf">74240</a>
<td>Octal buffer, inverting outputs</td>
</tr>

<tr class="anchor" id="74241">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74241.pdf">74241</a>
<td>Octal buffer, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74244">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74244.pdf">74244</a>
<td>Octal buffer, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74245">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74245.pdf">74245</a>
<td>Octal bus transceiver, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74251">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74251.pdf">74251</a>
<td>8-line to 1-line data selector/multiplexer, complementary outputs</td>
</tr>

<tr class="anchor" id="74253">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74253.pdf">74253</a>
<td>Dual 4-line to 1-line data selector/multiplexer</td>
</tr>

<tr class="anchor" id="74257">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74257.pdf">74257</a>
<td>Quad 2-line to 1-line data selector/multiplexer, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74258">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74258.pdf">74258</a>
<td>Quad 2-line to 1-line data selector/multiplexer, inverting outputs</td>
</tr>

<tr class="anchor" id="74265">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74265.pdf">74265</a>
<td>Quad complementary output elements</td>
</tr>

<tr class="anchor" id="74266">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74266.pdf">74266</a>
<td>Quad 2-input XNOR gate</td>
</tr>

<tr class="anchor" id="74273">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74273.pdf">74273</a>
<td>8-bit register, asynchronous clear</td>
</tr>

<tr class="anchor" id="74279">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74279.pdf">74279</a>
<td>Quad set-reset latch</td>
</tr>

<tr class="anchor" id="74280">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74280.pdf">74280</a>
<td>9-bit odd/even parity bit generator/checker</td>
</tr>

<tr class="anchor" id="74283">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74283.pdf">74283</a>
<td>4-bit binary full adder (has carry in function)</td>
</tr>

<tr class="anchor" id="74290">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74290.pdf">74290</a>
<td>Decade counter (separate divide-by-2 and divide-by-5 sections)</td>
</tr>

<tr class="anchor" id="74293">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74293.pdf">74293</a>
<td>4-bit binary counter (separate divide-by-2 and divide-by-8 sections)</td>
</tr>

<tr class="anchor" id="74298">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74298.pdf">74298</a>
<td>Quad 2-input multiplexer, storage</td>
</tr>

<tr class="anchor" id="74324">
<td>74324</td>
<td>Voltage-controlled oscillator (or crystal controlled), enable input, complementary outputs</td>
</tr>

<tr class="anchor" id="74365">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74365.pdf">74365</a>
<td>Hex buffer, non-inverting outputs</td>
</tr>

<tr class="anchor" id="74368">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74368.pdf">74368</a>
<td>Hex buffer, inverting outputs</td>
</tr>

<tr class="anchor" id="74373">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74373.pdf">74373</a>
<td>Octal transparent latch</td>
</tr>

<tr class="anchor" id="74374">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74374.pdf">74374</a>
<td>Octal register</td>
</tr>

<tr class="anchor" id="74375">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74375.pdf">74375</a>
<td>Quad bistable latch</td>
</tr>

<tr class="anchor" id="74377">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74377.pdf">74377</a>
<td>8-bit register, clock enable</td>
</tr>

<tr class="anchor" id="74390">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74390.pdf">74390</a>
<td>Dual 4-bit decade counter</td>
</tr>

<tr class="anchor" id="74393">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74393.pdf">74393</a>
<td>Dual 4-bit binary counter</td>
</tr>

<tr class="anchor" id="74399">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74399.pdf">74399</a>
<td>Quad 2-input multiplexer, storage</td>
</tr>

<tr class="anchor" id="74490">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74490.pdf">74490</a>
<td>Dual decade counter</td>
</tr>

<tr class="anchor" id="74540">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74540.pdf">74540</a>
<td>Octal inverter gate</td>
</tr>

<tr class="anchor" id="74541">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74541.pdf">74541</a>
<td>Octal buffer gate</td>
</tr>

<tr class="anchor" id="74595">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74595.pdf">74595</a>
<td>8-bit shift registers with 3-state output registers</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74629.pdf">74629</a>
<td>Dual voltage-controlled oscillator, enable control, range control</td>
</tr>

<tr class="anchor" id="74640">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74640.pdf">74640</a>
<td>Octal bus transceiver, inverting outputs</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74641.pdf">74641</a>
<td>Octal bus transceiver, non-inverting outputs</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74642.pdf">74642</a>
<td>Octal bus transceiver, inverting outputs</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/74645.pdf">74645</a>
<td>Octal bus transceivers with non-inverted 3-state outputs</td>
</tr>

</tbody>
</table>

The 7400-series table is based on the <a href="https://en.wikipedia.org/wiki/List_of_7400-series_integrated_circuits#Larger_footprints" target="_blank">complete list on Wikipedia</a>.

# 7500-series

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75160a.pdf">75160a</a></td>
<td>Octal general-purpose interface bus transceivers</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75162.pdf">75162</a></td>
<td>Octal general-purpose interface bus transceivers</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75176.pdf">75176</a></td>
<td>Differential bus transceiver</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75c188.pdf">75C188</a></td>
<td>Quadruple low-power line driver</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75451.pdf">75451</a></td>
<td>Dual-peripheral driver for high-current, high-speed switching</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75491.pdf">75491</a></td>
<td>Quad segment driver and hex digit driver for interfacing between mos and LED-displays</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/75492.pdf">75492</a></td>
<td>Quad segment driver and hex digit driver for interfacing between mos and LED-displays</td>
</tr>

</tbody>
</table>

# Converters

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor" id="ad1d12a10">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ad1d12a10.pdf">AD1D12A10</a></td>
<td>Astec DC-DC converter</td>
</tr>

<tr class="anchor" id="addac80">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/addac80.pdf">ADDAC80</a></td>
<td>Analog Devices - 12 bit D/A converter</td>
</tr>

</tbody>
</table>

# EPROMs

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/b1702a.pdf">B1702A</a></td>
<td>256-word x 8-bit. Vpp = -45V. This is one of the weirdest EPROMs (made by Intel). We suspect it's a really old version that was early on the market. Vpp is called Vgg and has very weird voltage. It only has 256 bytes of memory and initially stores 0s instead of 1s which is quite the opposite to how other EPROMs work. This is for the museum!</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/s6834.pdf">S6834</a></td>
<td>512-word x 8-bit. Also a really old chip from AMI (American Microsystems). The datasheet is incomplete and only two pages long.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/b2758.pdf">B2758</a></td>
<td>1024-word x 8-bit. Vpp = 26.5V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/c2708.pdf">C2708</a></td>
<td>1024-word x 8-bit. Also a super old chip from 1975 and instructions are unclear. Further research is required to find programming instructions. The datasheet states that `The programming specifications are described in the Data Catalog PROM/ROM Programming Instructions Section`. Vpp = 26V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn462716g.pdf">HN462716G</a></td>
<td>2048-word x 8-bit (there's also a -1 variant). Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/m5l2716k.pdf">M5L2716K</a></td>
<td>2048-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tms2516jl.pdf">TMS2516JL</a></td>
<td>2048-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/d2716.pdf">D2716</a></td>
<td>2048-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mb8516.pdf">MB8516</a></td>
<td>2048-word x 8-bit. Datasheet in Japanese and only two pages. Difficult to find good information.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn482732ag.pdf">HN482732AG</a></td>
<td>4096-word x 8-bit (-20 and -30 variants). Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn462532.pdf">HN462532</a></td>
<td>4096-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn462732.pdf">HN462732</a></td>
<td>4096-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tms2532.pdf">TMS2532JDL</a></td>
<td>4096-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mbm2732a.pdf">MBM2732A</a></td>
<td>4096-word x 8-bit. Vpp = 21V or 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/d2764.pdf">D2764</a></td>
<td>4096-word x 8-bit. Vpp = 21V. Exceeding 22V will cause permanent damage.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn27c64g.pdf">HN27C64G</a></td>
<td>8192-word x 8-bit (-15 and -20 variants). Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn482764g.pdf">HN482764G</a></td>
<td>8192-word x 8-bit. Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/nmc27c64.pdf">NMC27C64</a></td>
<td>8192-word x 8-bit. Vpp = 13V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tms2564.pdf">TMS2564</a></td>
<td>8192-word x 8-bit. Vpp = 25V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/s27c64a.pdf">S27C64A</a></td>
<td>8192-word x 8-bit. Vpp = 12.5V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mbm2764.pdf">MBM2764</a></td>
<td>8192-word x 8-bit. Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn4827128g.pdf">HN4827128G</a></td>
<td>16384-word x 8-bit. Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/m5l27128.pdf">M5L27128K</a></td>
<td>16384-word x 8-bit. Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mbm27128.pdf">MBM27128</a></td>
<td>16384-word x 8-bit. Vpp = 21V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn27256g.pdf">HN27256G</a></td>
<td>32768-word x 8-bit. Vpp = 12.5V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/nmc27c256.pdf">NMC27C256</a></td>
<td>32768-word x 8-bit. Vpp = 12.75V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/tms27c512jl.pdf">TMS27C512JL</a></td>
<td>65536-word x 8-bit. Vpp = 13V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mbm27c512.pdf">MBM27C512</a></td>
<td>65536-word x 8-bit. Vpp = 12.5V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/d27512.pdf">D27512</a></td>
<td>65536-word x 8-bit. Vpp = 12V. Exceeding 14V will cause permanent damage.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hn27c101g.pdf">HN27C101G</a></td>
<td>131072-word x 8-bit. Vpp = 12.5V.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/d27011-250v05.pdf">D27011</a></td>
<td>131072-word x 8-bit. Organized as 8 pages of 16K 8-bit words. Vpp = 12.74V (exceeding 14V will cause permanent damage).</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/m27c4001.pdf">M27C4001</a></td>
<td>524288-word x 8-bit (512Kbit). Vpp = 12.75V.</td>
</tr>

</tbody>
</table>

# MCUs

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor" id="ef6809">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ef6809.pdf">EF6809</a></td>
<td>8-bit microprocessing unit</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hd63701v0.pdf">HD63701VOC</a></td>
<td>8-bit MCU that also contains an EPROM. Can be programmed and erased by the same procedure as that of 27C256 or 27256.</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mc68hc705c8.pdf">MC68HC705C8</a></td>
<td>The MC68HC705C8 (HCMOS) microcontroller unit is a member of the M68HC05 family of microcontrollers.</td>
</tr>

</tbody>
</table>

# RAM

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor" id="hd68b44p">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hd68b44p.pdf">HD68B44P</a></td>
<td>Direct Memory Access Controller (DMAC)</td>
</tr>

<tr class="anchor" id="hm4864p">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm4864p.pdf">HM4864P</a></td>
<td>DRAM</td>
</tr>

<tr class="anchor" id="hm50256p">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm50256p.pdf">HM50256P</a></td>
<td>DRAM (-12 and -15 variants)</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm511000.pdf">HM511000</a></td>
<td>1048576-word x 1-bit CMOS DRAM</td>
</tr>

<tr class="anchor" id="hm6116p">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm6116p.pdf">HM6116P</a></td>
<td>2048-word x 8-bit high speed cmos static ram (-2, -3 and -4 variants)</td>
</tr>

<tr class="anchor" id="hm62256a">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm62256a.pdf">HM62256A</a></td>
<td>32768-word x 8-bit high speed cmos static ram</td>
</tr>

<tr class="anchor" id="hm62264a">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm62264a.pdf">HM62264A</a></td>
<td>8192-word x 8-bit high speed cmos static ram</td>
</tr>

<tr class="anchor" id="hm628128">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/hm628128.pdf">HM628128</a></td>
<td>131072-word x 8-bit high speed cmos static ram</td>
</tr>

</tbody>
</table>

# Misc ICs

<table>
<thead><tr>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Part number</th>
<th class="headerSort" tabindex="0" role="columnheader button" title="Sort ascending">Description</th>
</th></tr></thead><tbody>

<tr class="anchor" id="555">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/555.pdf">555</a></td>
<td>For timer, delay, pulse generation and oscillation applications</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/dm8131.pdf">DM8131</a></td>
<td>6-bit unified bus comparator</td>
</tr>

<tr class="anchor" id="ds1216">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ds1216.pdf">DS1216</a></td>
<td>SmartWatch RAM</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ds1488.pdf">DS1488</a></td>
<td>Quad line driver</td>
</tr>

<tr class="anchor">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/ds1489.pdf">DS1489</a></td>
<td>Quad line receiver</td>
</tr>

<tr class="anchor" id="lm324n">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/lm324n.pdf">LM324N</a></td>
<td>Low power quad operational amplifiers</td>
</tr>

<tr class="anchor" id="lm339n">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/lm339n.pdf">LM339N</a></td>
<td>Quad single supply comparators</td>
</tr>

<tr class="anchor" id="mc146818">
<td><a rel="nofollow" target="_blank" class="external text" href="datasheets/mc146818.pdf">MC146818</a></td>
<td>Real-time clock plus ram (RTC)</td>
</tr>

</tbody>
</table>
